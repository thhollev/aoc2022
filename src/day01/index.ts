import run from "aocrunner";
import { stringToList, mapListToNumbers } from "../utils/index.js";

const parseInput = (rawInput: string) => {
  return mapListToNumbers(stringToList(rawInput));
};

const part1 = (rawInput: string) => {
  const input = parseInput(rawInput);
  const numbersList: Array<number> = [];

  let max = 0;
  let i = 0;

  for (let num of input) {
    if (numbersList[i] === undefined) {
      numbersList[i] = 0;
    }
    if (num !== 0) {
      numbersList[i] += num;
    } else {
      if (numbersList[i - 1] > max) {
        max = numbersList[i - 1];
      }
      i++;
    }
  }

  return max;
};

const part2 = (rawInput: string) => {
  const input = parseInput(rawInput);
  const numbersList: Array<number> = [];

  let i = 0;
  for (let num of input) {
    if (numbersList[i] === undefined) {
      numbersList[i] = 0;
    }
    if (num !== 0) {
      numbersList[i] += num;
    } else {
      i++;
    }
  }

  const sortedNumbersList = numbersList.sort((a, b) => b - a);
  const sumOfThree =
    sortedNumbersList[0] + sortedNumbersList[1] + sortedNumbersList[2];

  return sumOfThree;
};

run({
  part1: {
    solution: part1,
  },
  part2: {
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});
