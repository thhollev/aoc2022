import run from "aocrunner";
import { stringToList } from "../utils/index.js";

type Stacks = { [key: number]: string[] };

const parseInput = (rawInput: string) => {
  const input = stringToList(rawInput);
  const separator = input.indexOf("");
  const stackLine = separator - 1;
  const stacks: Stacks = {};
  for (let i = 0; i < stackLine; i += 1) {
    const line = input[i].replaceAll(/\[|\]/g, " ").split("");
    for (let j = 0; j < line.length; j += 1) {
      if (line[j] !== " ") {
        const stackNum = parseInt(input[stackLine][j]);
        if (stackNum) {
          stacks[stackNum] = [line[j], ...(stacks[stackNum] || [])];
        }
      }
    }
  }
  const moves = input.slice(separator + 1).map((str) =>
    str
      .replace(/move ([0-9]*) from ([0-9]*) to ([0-9]*)/g, "$1,$2,$3")
      .split(",")
      .map((c) => parseInt(c)),
  );

  return { stacks, moves };
};

const part1 = (rawInput: string) => {
  const { stacks, moves } = parseInput(rawInput);
  for (let move of moves) {
    for (let i = 0; i < move[0]; i += 1) {
      const popped = stacks[move[1]].pop() as string;
      stacks[move[2]].push(popped);
    }
  }
  let buf = "";
  Object.values(stacks).forEach((value) => {
    buf += value[value.length - 1];
  });
  return buf;
};

const part2 = (rawInput: string) => {
  const { stacks, moves } = parseInput(rawInput);
  for (let move of moves) {
    const acc = [];
    for (let i = 0; i < move[0]; i += 1) {
      const popped = stacks[move[1]].pop() as string;
      acc.push(popped);
    }
    stacks[move[2]] = stacks[move[2]].concat(acc.reverse());
  }
  let buf = "";
  Object.values(stacks).forEach((value) => {
    buf += value[value.length - 1];
  });
  return buf;
};

run({
  part1: {
    tests: [
      // {
      //   input: ``,
      //   expected: "",
      // },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      // {
      //   input: ``,
      //   expected: "",
      // },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});
