import run from "aocrunner";
import { stringToList } from "../utils/index.js";

const parseInput = (rawInput: string) => {
  return stringToList(rawInput);
};

const part1 = (rawInput: string) => {
  const input = parseInput(rawInput);
  let count = 0;
  for (let string of input) {
    const arr = string.split("");
    const middle = Math.floor(arr.length / 2);
    const left = arr.slice(0, middle);
    const right = arr.slice(middle);
    const unique = left.filter((char) => right.includes(char))[0];
    count +=
      unique === unique.toLowerCase()
        ? unique.charCodeAt(0) - 96
        : unique.charCodeAt(0) - 38;
  }
  return count;
};

const part2 = (rawInput: string) => {
  const input = parseInput(rawInput);
  let count = 0;
  for (let i = 0; i <= input.length - 3; i += 3) {
    const parts = [
      input[i].split(""),
      input[i + 1].split(""),
      input[i + 2].split(""),
    ];

    const unique = parts[0].filter(
      (char) => parts[1].includes(char) && parts[2].includes(char),
    )[0];

    count +=
      unique === unique.toLowerCase()
        ? unique.charCodeAt(0) - 96
        : unique.charCodeAt(0) - 38;
  }
  return count;
};

run({
  part1: {
    tests: [
      {
        input: `
        vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
       `,
        expected: 157,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `
        vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
       `,
        expected: 70,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});
