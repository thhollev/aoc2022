import run from "aocrunner";
import { stringToList, range } from "../utils/index.js";

const parseInput = (rawInput: string) => {
  return stringToList(rawInput).map((str) =>
    str.split(/,|-/).map((char) => parseInt(char)),
  );
};

const part1 = (rawInput: string) => {
  const input = parseInput(rawInput);
  let count = 0;
  for (let [s1, e1, s2, e2] of input) {
    if ((s1 <= s2 && e1 >= e2) || (s1 >= s2 && e1 <= e2)) {
      count += 1;
    }
  }
  return count;
};

const part2 = (rawInput: string) => {
  const input = parseInput(rawInput);
  let count = 0;
  for (let [s1, e1, s2, e2] of input) {
    if (!(e1 < s2 || e2 < s1)) {
      count += 1;
    }
  }
  return count;
};

run({
  part1: {
    tests: [
      {
        input: `2-4,6-8
         2-3,4-5
         5-7,7-9
         2-8,3-7
         6-6,4-6
         2-6,4-8`,
        expected: 2,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `2-4,6-8
         2-3,4-5
         5-7,7-9
         2-8,3-7
         6-6,4-6
         2-6,4-8`,
        expected: 4,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});
