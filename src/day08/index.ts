import run from "aocrunner";
import { stringToNumList } from "../utils/index.js";

const parseInput = (rawInput: string) => {
  return stringToNumList(rawInput);
};

const part1 = (rawInput: string) => {
  const input = parseInput(rawInput);
  let totalVisible = input.length * 2 + input[0].length * 2 - 4;
  for (let i = 1; i < input.length - 1; i++) {
    for (let j = 1; j < input[0].length - 1; j++) {
      const current = input[i][j];

      // Look left
      let visible = true;
      for (let left = j - 1; left >= 0; left--) {
        if (input[i][left] >= current) {
          visible = false;
          break;
        }
      }
      if (visible) {
        totalVisible++;
        continue;
      }

      // Look right
      visible = true;
      for (let right = j + 1; right < input[0].length; right++) {
        if (input[i][right] >= current) {
          visible = false;
          break;
        }
      }
      if (visible) {
        totalVisible++;
        continue;
      }

      // Look up
      visible = true;
      for (let up = i - 1; up >= 0; up--) {
        if (input[up][j] >= current) {
          visible = false;
          break;
        }
      }
      if (visible) {
        totalVisible++;
        continue;
      }

      // Look down
      visible = true;
      for (let down = i + 1; down < input.length; down++) {
        if (input[down][j] >= current) {
          visible = false;
          break;
        }
      }
      if (visible) {
        totalVisible++;
        continue;
      }
    }
  }
  return totalVisible;
};

const part2 = (rawInput: string) => {
  const input = parseInput(rawInput);
  let highestScore = 0;
  for (let i = 1; i < input.length - 1; i++) {
    for (let j = 1; j < input[0].length - 1; j++) {
      const current = input[i][j];
      let leftScore = 0;
      let rightScore = 0;
      let upScore = 0;
      let downScore = 0;

      // Look left
      for (let left = j - 1; left >= 0; left--) {
        leftScore++;
        if (input[i][left] >= current) break;
      }

      // Look right
      for (let right = j + 1; right < input[0].length; right++) {
        rightScore++;
        if (input[i][right] >= current) break;
      }

      // Look up
      for (let up = i - 1; up >= 0; up--) {
        upScore++;
        if (input[up][j] >= current) break;
      }

      // Look down
      for (let down = i + 1; down < input.length; down++) {
        downScore++;
        if (input[down][j] >= current) break;
      }

      const totalScore = leftScore * rightScore * upScore * downScore;
      if (totalScore > highestScore) highestScore = totalScore;
    }
  }
  return highestScore;
};

run({
  part1: {
    tests: [
      {
        input: `30373
        25512
        65332
        33549
        35390`,
        expected: 21,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `30373
        25512
        65332
        33549
        35390`,
        expected: 8,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});
