/**
 * Root for your util libraries
 */

export const stringToList = (buffer: String) => {
  return buffer
    .replace(/\n$/g, "")
    .split("\n")
    .map((str) => str.trim());
};

export const stringToCharList = (buffer: String) => {
  return buffer
    .replace(/\n$/g, "")
    .split("\n")
    .map((str) => str.trim().split(""));
};

export const stringToNumList = (buffer: String) => {
  return buffer
    .replace(/\n$/g, "")
    .split("\n")
    .map((str) =>
      str
        .trim()
        .split("")
        .map((char) => parseInt(char)),
    );
};

export const mapListToNumbers = (list: string[]) => {
  return list.map((string) => Number(string));
};

export const clearSpaces = (list: string[]): string[] => {
  return list.map((string) => string.replace(/ /g, ""));
};

export const range = (
  start: number,
  stop?: number,
  step?: number,
): number[] => {
  if (typeof stop == "undefined") {
    stop = start;
    start = 0;
  }

  if (typeof step == "undefined") {
    step = 1;
  }

  if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
    return [];
  }

  const result = [];
  for (let i = start; step > 0 ? i < stop : i > stop; i += step) {
    result.push(i);
  }

  return result;
};
