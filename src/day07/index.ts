import run from "aocrunner";
import { stringToList } from "../utils/index.js";

const parseInput = (rawInput: string) => {
  const input = stringToList(rawInput).map((s) => s.trim());

  const sizeMap: Record<string, number> = {};
  let path = ["/"];

  for (const line of input) {
    if (line.startsWith("$ cd /")) {
      path = ["/"];
    } else if (line.startsWith("$ cd ..")) {
      path.pop();
    } else if (line.startsWith("$ cd ")) {
      path.push(line.slice(5));
    } else if (line.match(/^[0-9]/)) {
      const [size] = line.split(" ");
      for (const [i, chunk] of path.entries()) {
        if (!sizeMap[path.slice(0, i) + chunk]) {
          sizeMap[path.slice(0, i) + chunk] = 0;
        }
        sizeMap[path.slice(0, i) + chunk] += parseInt(size);
      }
    }
  }

  return sizeMap;
};

`
enum NodeType {
  DIRECTORY = "directory",
  FILE = "file",
}

interface NodeMeta {
  size?: number;
}

interface Node {
  name: string;
  type: NodeType;
  meta?: NodeMeta;
  children?: Node[];
  parent?: Node;
}

const traverseSystem = (node: Node, lines: string[], lineIxd: number): Node => {
  if (lines.length === lineIxd) {
    return node;
  }

  const line = lines[lineIxd];
  console.log(line);
  console.log(node);

  if (line.startsWith("$")) {
    const c = line.split(" ");
    if (c[1] === "ls") {
      return traverseSystem(node, lines, lineIxd + 1);
    } else {
      if (c[2] !== "..") {
        return traverseSystem(
          node.children?.find(
            (child) => child.type === NodeType.DIRECTORY && child.name === c[2],
          ) as Node,
          lines,
          lineIxd + 1,
        );
      } else {
        // Stuck here what the parent node?
        return traverseSystem(node, lines, lineIxd + 1);
      }
    }
  } else {
    const s = line.split(" ");
    node.children = [
      ...(node?.children ? node.children : []),
      {
        name: s[1],
        type: s[0] === "dir" ? NodeType.DIRECTORY : NodeType.FILE,
        meta: s[0] === "dir" ? {} : { size: parseInt(s[0]) },
      },
    ];
  }
  return traverseSystem(node, lines, lineIxd + 1);
};
`;

const part1 = (rawInput: string) => {
  const sizeMap = parseInput(rawInput);
  let acc = 0;

  for (const [_, value] of Object.entries(sizeMap)) {
    if (value <= 100000) acc += value;
  }

  return acc;
};

const part2 = (rawInput: string) => {
  const sizeMap = parseInput(rawInput);
  const x = (70000000 - sizeMap["/"] - 30000000) * -1;
  return Object.values(sizeMap).sort(
    (a, b) => Math.abs(x - a) - Math.abs(x - b),
  )[0];
};

run({
  part1: {
    tests: [
      {
        input: `$ cd /
       $ ls
       dir a
       14848514 b.txt
       8504156 c.dat
       dir d
       $ cd a
       $ ls
       dir e
       29116 f
       2557 g
       62596 h.lst
       $ cd e
       $ ls
       584 i
       $ cd ..
       $ cd ..
       $ cd d
       $ ls
       4060174 j
       8033020 d.log
       5626152 d.ext
       7214296 k`,
        expected: 95437,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      // {
      //   input: ``,
      //   expected: "",
      // },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});
