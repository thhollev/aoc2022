import run from "aocrunner";
import { stringToList, clearSpaces } from "../utils/index.js";

const parseInput = (rawInput: string) => {
  return clearSpaces(stringToList(rawInput));
};

const rock = 0;
const paper = 1;
const scissors = 2;

const mapToInt: { [key: string]: number } = {
  A: rock,
  X: rock,
  B: paper,
  Y: paper,
  C: scissors,
  Z: scissors,
};

// 0 lost, 3 draw, 6 win

const points: { [key: string]: number } = {
  0: 1,
  1: 2,
  2: 3,
};

const winStrategy: { [key: number]: number } = {
  0: 1,
  1: 2,
  2: 0,
};

const loseStrategy: { [key: number]: number } = {
  0: 2,
  1: 0,
  2: 1,
};

const part1 = (rawInput: string) => {
  const input = parseInput(rawInput);
  let score = 0;

  for (let round of input) {
    const playerA = mapToInt[round[0]];
    const playerB = mapToInt[round[1]];

    score += points[playerB];

    if (loseStrategy[playerB] === playerA) {
      score += 6;
    } else if (winStrategy[playerB] !== playerA) {
      score += 3;
    }
  }
  return score;
};

const part2 = (rawInput: string) => {
  const input = parseInput(rawInput);
  let score = 0;

  for (let round of input) {
    const playerA = mapToInt[round[0]];
    const playerB = round[1];

    if (playerB === "X") {
      // should lose
      score += points[loseStrategy[playerA]];
    } else if (playerB === "Z") {
      // should win
      score += 6 + points[winStrategy[playerA]];
    } else {
      // should draw
      score += 3 + points[playerA];
    }
  }

  return score;
};

run({
  part1: {
    tests: [
      {
        input: `A Y
        B X
        C Z
      `,
        expected: 15,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `A Y
        B X
        C Z
      `,
        expected: 12,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});
