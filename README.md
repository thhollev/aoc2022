<!-- Entries between SOLUTIONS and RESULTS tags are auto-generated -->

[![AoC](https://badgen.net/badge/AoC/2022/blue)](https://adventofcode.com/2022)
[![Node](https://badgen.net/badge/Node/v16.13.0+/blue)](https://nodejs.org/en/download/)
[![Language](https://badgen.net/badge/Language/TypeScript/blue)](https://www.typescriptlang.org/)
[![Template](https://badgen.net/badge/Template/aocrunner/blue)](https://github.com/caderek/aocrunner)

# 🎄 Advent of Code 2022 🎄

## Solutions

<!--SOLUTIONS-->

[![Day](https://badgen.net/badge/01/%E2%98%85%E2%98%85/green)](src/day01)
[![Day](https://badgen.net/badge/02/%E2%98%85%E2%98%85/green)](src/day02)
[![Day](https://badgen.net/badge/03/%E2%98%85%E2%98%85/green)](src/day03)
[![Day](https://badgen.net/badge/04/%E2%98%85%E2%98%85/green)](src/day04)
[![Day](https://badgen.net/badge/05/%E2%98%85%E2%98%85/green)](src/day05)
[![Day](https://badgen.net/badge/06/%E2%98%85%E2%98%85/green)](src/day06)
[![Day](https://badgen.net/badge/07/%E2%98%85%E2%98%85/green)](src/day07)
[![Day](https://badgen.net/badge/08/%E2%98%85%E2%98%85/green)](src/day08)
![Day](https://badgen.net/badge/09/%E2%98%86%E2%98%86/gray)
![Day](https://badgen.net/badge/10/%E2%98%86%E2%98%86/gray)
![Day](https://badgen.net/badge/11/%E2%98%86%E2%98%86/gray)
![Day](https://badgen.net/badge/12/%E2%98%86%E2%98%86/gray)
![Day](https://badgen.net/badge/13/%E2%98%86%E2%98%86/gray)

<!--/SOLUTIONS-->

_Click a badge to go to the specific day._

---

## Installation

```
npm i
```

## Running in dev mode

```
npm start <day>
```

Example:

```
npm start 1
```

---

## Results

<!--RESULTS-->

```
Day 01
Time part 1: 0.36ms
Time part 2: 0.376ms
Both parts: 0.7355ms
```

```
Day 02
Time part 1: 0.546ms
Time part 2: 0.71ms
Both parts: 1.255875ms
```

```
Day 03
Time part 1: 0.534ms
Time part 2: 0.509ms
Both parts: 1.0436670000000001ms
```

```
Day 04
Time part 1: 0.945ms
Time part 2: 0.875ms
Both parts: 1.820668ms
```

```
Day 05
Time part 1: 0.847ms
Time part 2: 0.831ms
Both parts: 1.677708ms
```

```
Day 06
Time part 1: 0.29ms
Time part 2: 3.357ms
Both parts: 3.646624ms
```

```
Day 07
Time part 1: 1.775ms
Time part 2: 2.408ms
Both parts: 4.183125ms
```

```
Day 08
Time part 1: 5.615ms
Time part 2: 45.044ms
Both parts: 50.658666ms
```

```
Day 09
Time part 1: -
Time part 2: -
Both parts: -
```

```
Day 10
Time part 1: -
Time part 2: -
Both parts: -
```

```
Day 11
Time part 1: -
Time part 2: -
Both parts: -
```

```
Day 12
Time part 1: -
Time part 2: -
Both parts: -
```

```
Day 13
Time part 1: -
Time part 2: -
Both parts: -
```

```
Total stars: 16/50
Total time: 65.022ms
```

<!--/RESULTS-->

---

✨🎄🎁🎄🎅🎄🎁🎄✨
